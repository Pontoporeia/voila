# voila_webplatforme 📹 🔀

The website that will host the Voilà Project created collaboratively between J K T.

**Live**: https://voilà.happyngreen.fr

Built in vanilla html, css and javascript and aims to be low-tech and eco-responsable.

The map is created using the [leaflet api](https://leafletjs.com/) and following this [example by asmaloney](https://asmaloney.com/2014/01/code/creating-an-interactive-map-with-leaflet-and-openstreetmap/) without the jquery.
[Here](https://github.com/asmaloney/Leaflet_Example) is the github project.

TODO: 
- initial structure
- working map
- markers lead to video pages
- SSG implementation
