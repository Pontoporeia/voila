// See post: https://asmaloney.com/2014/01/code/creating-an-interactive-map-with-leaflet-and-openstreetmap/

var map = L.map('map', {
    center: [50.8386, 4.3504],
    minZoom: 11,
    zoom: 11,
    touchZoom: true,
  })


  var tilemap = 'https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg'

  // https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png

  L.tileLayer(tilemap, {
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    subdomains: ['a', 'b', 'c'],
  }).addTo(map)


  for (var i = 0; i < markers.length; ++i) {
    
    var myIcon = L.icon({
      iconUrl: markers[i].ep_marker,
      // iconRetinaUrl:'img/pin48.png',
      iconSize: [30, 30],
      iconAnchor: [9, 21],
      popupAnchor: [0, -14],
      className: 'dot'
    })

    L.marker([markers[i].lat, markers[i].lng], { icon: myIcon })
      .bindPopup(
        '<a href="' +
          markers[i].video_url +
          '" target="_blank">'+ markers[i].ep_emoji +
          ' Ep' + 
          markers[i].ep_num + 
          '↪' +
          markers[i].ep_name +
          '</a></br></br><a href="' +
          markers[i].url +
          '" target="_blank"> 🚀↝Website</a> <a href="' +
          markers[i].osm_url +
          '" target="_blank">🪧↝Location</a>'
      )
      .addTo(map)
  }
