var markers = [
    {
        lat: 50.80328,
        lng: 4.32210,
        ep_name: "Green Fabric",
        ep_emoji: "🪡",
        video_url: "video/video.html",
        url: "https://greenfabric.be/",
        osm_url: "https://osm.org/go/0EoSCPZtc?way=247791508",
        ep_marker: "img/1FAA1.svg"
    },
    {
        lat: 50.83232,
        lng: 4.36541,
        ep_name: "La Grainothèque Mobile",
        ep_emoji: "🌱",
        video_url: "video/video.html",
        url: "https://linktr.ee/Grainotheque_mobile_dIxelles",
        osm_url: "https://osm.org/go/0EoSzZ9G_?node=2792159984",
        ep_marker: "img/1F33F.svg"
    },
    {
        lat: 50.85401,
        lng: 4.35017,
        ep_name: "OSM Bruxelles-Grand Hospice",
        ep_emoji: "🌍",
        video_url: "video/video.html",
        url: "https://matrix.to/#/#osmbe:matrix.org",
        osm_url: "https://osm.org/go/0EoThQVml?node=9119366785",
        ep_marker: "img/1F30D.svg"
    },
    {
        lat: 50.8282269,
        lng: 4.3691757,
        ep_name: "Nubo + Domain Public",
        ep_emoji: "📡",
        video_url: "video/video.html",
        url: "https://www.domainepublic.net/",
        osm_url: "https://osm.org/go/0EoSy31cK?node=5828922266",
        ep_marker: "img/1F4E1.svg"
    },
    {
        lat: 50.85162,
        lng: 4.3616,
        ep_name: "OSP",
        ep_emoji: "💻",
        video_url: "video/video.html",
        url: "http://osp.kitchen/",
        osm_url: "https://osm.org/go/0EoTjDEEZ--?way=30145405",
        ep_marker: "img/1F5A5.svg"
    },
]
// https://www.grand-hospice.brussels/